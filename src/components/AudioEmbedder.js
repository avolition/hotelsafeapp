// import packages
import React from 'react'
import PropTypes from 'prop-types'

const AudioEmbedder = props => {
  const audioElements = props.audioFiles.map(file => (
    <audio key={file.id} id={file.id} src={file.src} />
  ))
  return <div>{audioElements}</div>
}

Audio.propTypes = {
  audioFiles: PropTypes.array.isRequired
}

export default AudioEmbedder
