// import packages
import React from 'react'
import PropTypes from 'prop-types'
// import helper files
import { INVALID, spreadText } from '../js/utils'

const Display = props => {
  const value = props.value === INVALID ? INVALID : spreadText(props.value)
  return (
    <div className="display">
      <input className="textDisplay" type="text" value={value} maxLength="7" readOnly />
      <div className={'light ' + (props.pin ? 'red' : '')} />
    </div>
  )
}

Display.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  pin: PropTypes.string.isRequired
}

export default Display
