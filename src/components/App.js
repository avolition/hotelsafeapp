// import packages
import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
// import components
import Keypad from './Keypad'
import Display from './Display'
import AudioEmbedder from './AudioEmbedder'
// import helper files
import { INVALID } from '../js/utils'
// import assets
import lockPNG from '../assets/favicon/lock.png'
import unlockPNG from '../assets/favicon/unlock.png'
import keySND from '../assets/sounds/key.wav'
import lockSND from '../assets/sounds/lock.mp3'
import unlockSND from '../assets/sounds/unlock.wav'
import invalidSND from '../assets/sounds/invalid.wav'

const audioInfo = [
  { id: 'keySND', src: keySND },
  { id: 'lockSND', src: lockSND },
  { id: 'unlockSND', src: unlockSND },
  { id: 'invalidSND', src: invalidSND }
]

class App extends Component {
  state = {
    pin: '', // (empty string = unlocked)
    value: '',
    keysToDisplay: [1, 2, 3, 4, 5, 6, 7, 8, 9, 'CLR', 0, '⇨']
  }
  playSound = sound => {
    const audio = document.getElementById(`${sound}`)
    audio.currentTime = 0
    audio.play()
  }
  // fn to triage key press or click
  handleInput = (e, value = undefined) => {
    let char
    if (e.type === 'click') {
      char = value
    } else if (e.type === 'keydown') {
      switch (e.key) {
        case 'Escape':
        case 'Backspace':
          char = 'CLR'
          break
        case 'Enter':
          char = '⇨'
          break
        default:
          // if a number is pressed, save it to char
          if (!isNaN(parseInt(e.key, 10))) {
            char = e.key
          }
      }
    }
    // only pass char if it is a number or 'CLR' or '⇨'
    if (!isNaN(parseInt(char, 10)) || char === 'CLR' || char === '⇨') {
      this.handleChange(char)
    }
  }
  // fn to test form value
  handleChange = char => {
    const { value } = this.state
    // if char is a number
    if (!isNaN(parseInt(char, 10))) {
      // & existing form value less than 4 digits, add digit to form value state
      if (value.length < 4) {
        this.setState({ value: `${value}${char}` })
        this.playSound('keySND')
      }
      // & existing form value is INVALID, reset form value state with 1st char
      if (value === INVALID) {
        this.setState({ value: `${char}` })
        this.playSound('keySND')
      }
      return
    }
    // if not a number, testPin or clear
    switch (char) {
      case '⇨':
        this.testPin()
        break
      default:
        this.setState({ value: '' })
        return this.playSound('keySND')
    }
  }
  // fn to set / test pin
  testPin = () => {
    const { pin, value } = this.state
    // if no pin and pin is 4 digits, set pin and clear form value (if unlocked, lock)
    if (!pin && value.length === 4) {
      this.setState({ pin: value, value: '' })
      return this.playSound('lockSND')
    }
    // if pin & form value = pin, clear pin & value (if locked, unlock)
    if (pin && value === pin) {
      this.setState({ pin: '', value: '' })
      return this.playSound('unlockSND')
    }
    // if pin (locked) & form value != pin, set form value to invalid (reject)
    if (pin && value !== pin) {
      this.setState({ value: 'INVALID' })
      return this.playSound('invalidSND')
    }
    this.setState({ value: '' })
    return this.playSound('keySND')
  }
  componentDidMount = () => {
    document.addEventListener('keydown', this.handleInput, false)
  }

  render() {
    const { keysToDisplay, value, pin } = this.state
    return (
      <form className="interface">
        <Keypad keysToDisplay={keysToDisplay} handleInput={this.handleInput} />
        <Display value={value} pin={pin} />
        <AudioEmbedder audioFiles={audioInfo} />
        <Helmet>
          <link id="dynamic-favicon" rel="shortcut icon" href={pin ? lockPNG : unlockPNG} />
        </Helmet>
      </form>
    )
  }
}

export default App
