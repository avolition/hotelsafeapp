// import packages
import React from 'react'
import PropTypes from 'prop-types'

const Key = props => (
  <input
    type="button"
    className="key"
    value={props.text}
    onClick={(e, value = props.text) => props.handleInput(e, value)}
  />
)

Key.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  handleInput: PropTypes.func.isRequired
}

export default Key
