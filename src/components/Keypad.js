// import packages
import React from 'react'
import PropTypes from 'prop-types'
// import components
import Key from './Key'

const Keypad = props => {
  const displayKeys = props.keysToDisplay.map(key => (
    <Key className="key" key={key} text={key} handleInput={props.handleInput} />
  ))
  return <div className="keypad">{displayKeys}</div>
}

Keypad.propTypes = {
  keysToDisplay: PropTypes.array.isRequired,
  handleInput: PropTypes.func.isRequired
}

export default Keypad
