// constant for cross App utilization
export const INVALID = 'INVALID'

// function to space numbers on text display
export function spreadText(str = '') {
  return [...str].join(' ')
}
