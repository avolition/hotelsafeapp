// import functions for testing
import { INVALID, spreadText } from '../js/utils'

export const handleInput = (e, value = undefined) => {
  let char
  if (e.type === 'click') {
    char = value
  } else if (e.type === 'keydown') {
    switch (e.key) {
      case 'Escape':
      case 'Backspace':
        char = 'CLR'
        break
      case 'Enter':
        char = '⇨'
        break
      default:
        // if a number is pressed, save it to char
        if (!isNaN(parseInt(e.key, 10))) {
          char = e.key
        }
    }
  }
  // only pass char if it is a number or 'CLR' or '⇨'
  if (!isNaN(parseInt(char, 10)) || char === 'CLR' || char === '⇨') {
    return char
  }
}

// initialize object for holding handleChange return values
const returnValues = {
  setStateValue: '',
  soundPlayed: '',
  testPinCalled: false,
  reset: function() {
    this.setStateValue = ''
    this.soundPlayed = ''
    this.testPinCalled = false
  }
}
export const handleChange = (char, initialState) => {
  returnValues.reset()
  const value = initialState
  // if char is a number
  if (!isNaN(parseInt(char, 10))) {
    // & existing form value less than 4 digits, add digit to form value state
    if (value.length < 4) {
      returnValues.setStateValue = `${value}${char}`
      returnValues.soundPlayed = 'keySND'
    }
    // & existing form value is INVALID, reset form value state with 1st char
    if (value === INVALID) {
      returnValues.setStateValue = `${char}`
      returnValues.soundPlayed = 'keySND'
    }
    // console.log('returned from fn: ', returnValues)
    return JSON.stringify(returnValues)
  }
  // if not a number, testPin or clear
  switch (char) {
    case '⇨':
      returnValues.testPinCalled = true
      break
    default:
      returnValues.setStateValue = ''
      returnValues.soundPlayed = 'keySND'
  }
  // console.log('returned from fn: ', returnValues)
  return JSON.stringify(returnValues)
}

// initialize object for holding testPin return values
const returnValues2 = {
  setStateValue: '',
  setStatePin: '',
  soundPlayed: '',
  reset: function() {
    this.setStateValue = ''
    this.setStatePin = ''
    this.soundPlayed = ''
  }
}
export const testPin = (pin, value) => {
  returnValues2.reset()
  // if no pin and pin is 4 digits, set pin and clear form value (if unlocked, lock)
  if (!pin && value.length === 4) {
    returnValues2.setStatePin = value
    returnValues2.setStateValue = ''
    returnValues2.soundPlayed = 'lockSND'
    // console.log('returned from fn: ', returnValues2)
    return JSON.stringify(returnValues2)
  }
  // if pin & form value = pin, clear pin & value (if locked, unlock)
  if (pin && value === pin) {
    returnValues2.setStatePin = ''
    returnValues2.setStateValue = ''
    returnValues2.soundPlayed = 'unlockSND'
    // console.log('returned from fn: ', returnValues2)
    return JSON.stringify(returnValues2)
  }
  // if pin (locked) & form value != pin, set form value to invalid (reject)
  if (pin && value !== pin) {
    returnValues2.setStatePin = pin
    returnValues2.setStateValue = 'INVALID'
    returnValues2.soundPlayed = 'invalidSND'
    // console.log('returned from fn: ', returnValues2)
    return JSON.stringify(returnValues2)
  }
  returnValues2.setStatePin = pin
  returnValues2.setStateValue = ''
  returnValues2.soundPlayed = 'keySND'
  // console.log('returned from fn: ', returnValues2)
  return JSON.stringify(returnValues2)
}
