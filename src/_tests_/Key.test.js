// import packages
import React from 'react'
import { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
// import Component(s) for testing
import Key from '../components/Key'

// setup spy for testing
const handleInput = jest.fn()
// store Key element for testing
const wrapper = shallow(<Key handleInput={handleInput} />)
const renderedComponent = renderer.create(<Key handleInput={handleInput} />).toJSON()

// renders without crashing
it('renders without crashing', () => {
  wrapper
})
// matches previous snapshot
it('matches previous snapshot', () => {
  expect(renderedComponent).toMatchSnapshot()
})
// be input element of type button || button element
it('is input element of type button OR button element', () => {
  expect(wrapper.containsAnyMatchingElements(['input', 'button']))
})
// has class of .key
it('has class of .key', () => {
  expect(wrapper.hasClass('key'))
})
// calls handleInput on click
it('calls handleInput on click', () => {
  const key = wrapper.find('.key')
  key.simulate('click')
  expect(handleInput).toBeCalled()
})
