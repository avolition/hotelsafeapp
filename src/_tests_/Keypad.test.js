// import packages
import React from 'react'
import ReactDOM from 'react-dom'
import { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
// import Component(s) for testing
import Keypad from '../components/Keypad'

// setup spy for testing
const handleInput = jest.fn()
// initialize props
const keysToDisplay = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'CLR', 0, '⇨']

// store Key element for testing
const wrapper = mount(<Keypad keysToDisplay={keysToDisplay} handleInput={handleInput} />)
const renderedComponent = renderer
  .create(<Keypad keysToDisplay={keysToDisplay} handleInput={handleInput} />)
  .toJSON()

// renders without crashing
it('renders without crashing', () => {
  wrapper
})
// matches previous snapshot
it('matches previous snapshot', () => {
  expect(renderedComponent).toMatchSnapshot()
})
// is a div element
it('is a div element', () => {
  expect(wrapper.containsMatchingElement('div'))
})
// has class of .keypad
it('has class of .keypad', () => {
  expect(wrapper.hasClass('keypad'))
})
// maps / renders all keys from keysToDisplay array
it('renders all keys from keysToDisplay array', () => {
  const allKeys = wrapper.find('Key')
  expect(allKeys.length).toEqual(keysToDisplay.length)
})
