// import packages
import React from 'react'
import { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
// import Component(s) for testing
import App from '../components/App'

// store element for testing
const wrapperShallow = shallow(<App />)
const wrapperMount = mount(<App />)
const renderedComponent = renderer.create(<App />).toJSON()

// renders without crashing
it('renders without crashing', () => {
  wrapperShallow
})
// matches previous snapshot
it('matches previous snapshot', () => {
  expect(renderedComponent).toMatchSnapshot()
})
// renders Keypad
it('renders Keypad', () => {
  expect(wrapperMount.containsMatchingElement('Keypad'))
})
// renders Display
it('renders Display', () => {
  expect(wrapperMount.containsMatchingElement('Display'))
})
// renders AudioEmbedder
it('renders AudioEmbedder', () => {
  expect(wrapperMount.containsMatchingElement('AudioEmbedder'))
})
// 'initial state values are pin: "" and value: ""'
it('initial state values are pin: "" and value: ""', () => {
  expect(wrapperMount.state('pin')).toEqual('') && expect(wrapperMount.state('value')).toEqual('')
})
