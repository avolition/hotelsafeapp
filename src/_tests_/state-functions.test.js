// import packages
import React from 'react'
// import functions for testing
import { handleInput, handleChange, testPin } from './state-functions'
import { INVALID } from '../js/utils'

////////////////////////////////////////////////////////////////////////////////
// handleInput tests
//////////////////////////////////////////////////////////////////////////////

// setup vars for testing
const clickEvent = {
  type: 'click'
}
const keydownEvent = {
  type: 'keydown'
}
// handleInput - click event with 0-9 values should return the input number
it('handleInput - click event with 0-9 values should return the input number', () => {
  for (let val = 0, y = 9; val <= y; val++) {
    expect(handleInput(clickEvent, val)).toBe(val)
  }
})
// handleInput - click event with 'CLR' value should return 'CLR'
it("handleInput - click event with 'CLR' value should return 'CLR'", () => {
  let val = 'CLR'
  expect(handleInput(clickEvent, val)).toBe(val)
})
// handleInput - click event with '⇨' value should return '⇨'
it("handleInput - click event with '⇨' value should return '⇨'", () => {
  let val = '⇨'
  expect(handleInput(clickEvent, val)).toBe(val)
})
// handleInput - keydown event with 0-9 key should return the input number
it('handleInput - keydown event with 0-9 key should return the input number', () => {
  for (let val = 0, y = 9; val <= y; val++) {
    keydownEvent.key = val
    expect(handleInput(keydownEvent)).toBe(val)
  }
})
// handleInput - keydown event with 'Escape' or 'Backspace' key should return 'CLR'
it("handleInput - keydown event with 'Escape' or 'Backspace' key should return 'CLR'", () => {
  keydownEvent.key = 'Escape'
  expect(handleInput(keydownEvent)).toBe('CLR')
  keydownEvent.key = 'Backspace'
  expect(handleInput(keydownEvent)).toBe('CLR')
})
// handleInput - keydown event with 'Enter' key should return '⇨'
it("handleInput - keydown event with 'Enter' key should return '⇨'", () => {
  keydownEvent.key = 'Enter'
  expect(handleInput(keydownEvent)).toBe('⇨')
})
// handleInput - other keys return undefined (nothing)
it('handleInput - other keys return undefined (nothing)', () => {
  for (let val = 0, y = 126; val <= y; val++) {
    if (val < 48 && val > 57) {
      keydownEvent.key = String.fromCharCode(val)
      expect(handleInput(keydownEvent)).toBe(undefined)
    }
  }
})

//////////////////////////////////////////////////////////////////////////////
// handleChange tests
//////////////////////////////////////////////////////////////////////////////

// initialize var to hold handleChange expected return values
const returnValues = {
  setStateValue: '',
  soundPlayed: '',
  testPinCalled: false,
  reset: function() {
    this.setStateValue = ''
    this.soundPlayed = ''
    this.testPinCalled = false
  }
}
// handleChange - when initialState is empty & number input, state[value] should be set to new char string & keySND played
it('handleChange - when initialState is empty & number input, state[value] should be set to new char string & keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = ''
  for (let char = 0, y = 9; char <= y; char++) {
    returnValues.setStateValue = `${initialState}${char}`
    // console.log('expected: ', returnValues)
    expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
  }
})
// handleChange - when initialState is empty & "CLR" input, state[value] should be empty & keySND played
it('handleChange - when initialState is empty & "CLR" input, state[value] should be empty & keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = ''
  let char = 'CLR'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState is empty & "⇨" input, testPin should be called
it('handleChange - when initialState is empty & "⇨" input, testPin should be called', () => {
  returnValues.reset()
  returnValues.testPinCalled = true
  let initialState = ''
  let char = '⇨'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState value is < 4 digits & number input, state[value] should have char appended & keySND played
it('handleChange - when initialState value is < 4 digits & number input, state[value] should have char appended & keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = '763'
  for (let char = 0, y = 9; char <= y; char++) {
    returnValues.setStateValue = `${initialState}${char}`
    // console.log('expected: ', returnValues)
    expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
  }
})
// handleChange - when initialState value is < 4 digits & "CLR" input, state[value] should be set to empty & keySND played
it('handleChange - when initialState value is < 4 digits & "CLR" input, state[value] should be set to empty & keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = '763'
  let char = 'CLR'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState value is < 4 digits & "⇨" input, testPin should fire
it('handleChange - when initialState value is < 4 digits & "⇨" input, testPin should fire', () => {
  returnValues.reset()
  returnValues.testPinCalled = true
  let initialState = '763'
  let char = '⇨'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState value is equal to 4 digits & "⇨" input, testPin should fire
it('handleChange - when initialState value is equal to 4 digits & "⇨" input, testPin should fire', () => {
  returnValues.reset()
  returnValues.testPinCalled = true
  let initialState = '7634'
  let char = '⇨'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState value is INVALID & number input, new char string should be returned + keySND played
it('handleChange - when initialState value is INVALID & number input, new char string should be returned + keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = INVALID
  for (let char = 0, y = 9; char <= y; char++) {
    returnValues.setStateValue = `${char}`
    // console.log('expected: ', returnValues)
    expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
  }
})
// handleChange - when initialState value is INVALID & "CLR input, state[value] should be "" & keySND played
it('handleChange - when initialState value is INVALID & "CLR input, state[value] should be "" & keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = INVALID
  let char = 'CLR'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState value is INVALID & "⇨" input, testPin should fire
it('handleChange - when initialState value is INVALID & "⇨" input, testPin should fire', () => {
  returnValues.reset()
  returnValues.testPinCalled = true
  let initialState = INVALID
  let char = '⇨'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})
// handleChange - when initialState value is < 4 digits & "CLR" input, state[value] should be empty & keySND played
it('handleChange - when initialState value is < 4 digits & "CLR" input, state[value] should be empty & keySND played', () => {
  returnValues.reset()
  returnValues.soundPlayed = 'keySND'
  let initialState = '763'
  let char = 'CLR'
  // console.log('expected: ', returnValues)
  expect(handleChange(char, initialState)).toBe(JSON.stringify(returnValues))
})

//////////////////////////////////////////////////////////////////////////////
// testPin tests
//////////////////////////////////////////////////////////////////////////////

// initialize var to hold testPin expected return values
const returnValues2 = {
  setStateValue: '',
  setStatePin: '',
  soundPlayed: '',
  reset: function() {
    this.setStateValue = ''
    this.setStatePin = ''
    this.soundPlayed = ''
  }
}
// testPin - if no pin and value is < 4 digits, state should be empty & keySND played
it('testPin - if no pin and value is < 4 digits, state should be empty & keySND played', () => {
  returnValues2.reset()
  returnValues2.soundPlayed = 'keySND'
  let initialPin = ''
  let initialVal = '763'
  // console.log('expected: ', returnValues2)
  expect(testPin(initialPin, initialVal)).toBe(JSON.stringify(returnValues2))
})
// testPin - if pin set and value is < 4 digits, state[pin] same & state[value] "INVALID" & invalidSND played
it('testPin - if pin set and value is < 4 digits, state[pin] same & state[value] "INVALID" & invalidSND played', () => {
  returnValues2.reset()
  returnValues2.setStateValue = 'INVALID'
  returnValues2.setStatePin = '0000'
  returnValues2.soundPlayed = 'invalidSND'
  let initialPin = '0000'
  let initialVal = '763'
  // console.log('expected: ', returnValues2)
  expect(testPin(initialPin, initialVal)).toBe(JSON.stringify(returnValues2))
})
// testPin - if no pin and value is 4 digits, state[pin] should be 4 digits & state[value] empty & lockSND played
it('testPin - if no pin and value is 4 digits, state[pin] should be 4 digits & state[value] empty & lockSND played', () => {
  returnValues2.reset()
  returnValues2.setStatePin = '0000'
  returnValues2.soundPlayed = 'lockSND'
  let initialPin = ''
  let initialVal = '0000'
  // console.log('expected: ', returnValues2)
  expect(testPin(initialPin, initialVal)).toBe(JSON.stringify(returnValues2))
})
// testPin - if pin set and value is different 4 digits, state[pin] same & state[value] "INVALID" & invalidSND played
it('testPin - if pin set and value is different 4 digits, state[pin] same & state[value] "INVALID" & invalidSND played', () => {
  returnValues2.reset()
  returnValues2.setStateValue = 'INVALID'
  returnValues2.setStatePin = '0000'
  returnValues2.soundPlayed = 'invalidSND'
  let initialPin = '0000'
  let initialVal = '7637'
  // console.log('expected: ', returnValues2)
  expect(testPin(initialPin, initialVal)).toBe(JSON.stringify(returnValues2))
})
// testPin - if pin set and value is same 4 digits, state should be empty & unlockSND played
it('testPin - if pin set and value is same 4 digits, state should be empty & unlockSND played', () => {
  returnValues2.reset()
  returnValues2.setStateValue = ''
  returnValues2.setStatePin = ''
  returnValues2.soundPlayed = 'unlockSND'
  let initialPin = '0000'
  let initialVal = '0000'
  // console.log('expected: ', returnValues2)
  expect(testPin(initialPin, initialVal)).toBe(JSON.stringify(returnValues2))
})
