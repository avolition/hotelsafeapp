// import packages
import React from 'react'
import { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
// import Component(s) for testing
import Display from '../components/Display'

// initialize props
const value = '0000'
const pin = ''

// store element for testing
const wrapper = mount(<Display value={value} pin={pin} />)
const renderedComponent = renderer.create(<Display value={value} pin={pin} />).toJSON()

// renders without crashing
it('renders without crashing', () => {
  wrapper
})
// matches previous snapshot
it('matches previous snapshot', () => {
  expect(renderedComponent).toMatchSnapshot()
})
// is a div element
it('is a div element', () => {
  expect(wrapper.containsMatchingElement('div'))
})
// has class of .display
it('has class of .display', () => {
  expect(wrapper.hasClass('display'))
})
// contains input element of type text
it('contains input element of type text', () => {
  expect(wrapper.find('input[text]'))
})
// contains input element with class of .textDisplay
it('contains input element with class of .textDisplay', () => {
  expect(wrapper.find('input').hasClass('display'))
})
// input element contains same text as value (with spaces between each digit)
it('input element contains same text as value (with spaces between each digit)', () => {
  expect(wrapper.find('input').prop('value')).toEqual([...value].join(' '))
})
// contains element with class of .light
it('contains element with class of .light', () => {
  expect(wrapper.find('.light'))
})
// contains element with class of .red when pin is not empty
it('contains element with class of .red when pin is not empty', () => {
  const wrapper = mount(<Display value={value} pin={'1111'} />)
  expect(wrapper.find('.light').hasClass('red'))
})
